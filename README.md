## Links ##

- Main Site: [http://skratchdot.com](http://skratchdot.com)

- Jekyll Sources: [https://github.com/skratchdot/skratchdot.com/](https://github.com/skratchdot/skratchdot.com/)

- Github Page: [https://github.com/skratchdot](https://github.com/skratchdot)


### Build Info ###

1. Clone the repo

        git clone https://github.com/skratchdot/skratchdot.com.git

2. Update modules

        npm install

3. Build site by running grunt:

        grunt

