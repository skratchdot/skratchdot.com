---
layout: page
title: Contact
---
# Contact #
<div class="center">
	If you have any questions, suggestions, or comments, you can send an email to the following address:
	<br /><br />
	<img width="400" border="0" height="110" title="j e f f - s k r a t c h d o t . c o m" alt="j e f f - s k r a t c h d o t . c o m" src="/images/email.png" />
	<br /><br />
	Or you can follow me:
	<br /><br />
	<a href="https://twitter.com/skratchdot" class="twitter-follow-button" data-show-count="false" data-size="large">Follow @skratchdot</a>
	<br /><br />
	You can also comment publicly below:
</div>
