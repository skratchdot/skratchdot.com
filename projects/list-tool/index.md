---
layout: "page"
title: "List Tool"
---
# List Tool

List Tool is a simple web application to compare lists. To see it in
action, visit:

- [List Tool](http://projects.skratchdot.com/list-tool/)

[![List Tool][2]][1]

  [1]: http://projects.skratchdot.com/list-tool/index.html
  [2]: http://projects.skratchdot.com/list-tool/img/preview.jpg (List Tool)


## List/Set Operations

- Self, no-op

- Union, Logical OR

- Intersection, Logical AND

- Set difference, Complement, Minus

- Symmetric difference, Logical XOR


## Other operations

- Sort

- Reverse

- Unique

- Trim

## Built with

- [Ace Editor](https://github.com/ajaxorg/ace/)

- [Bootstrap](http://twitter.github.com/bootstrap/)

- [jQuery](http://jquery.com/)

